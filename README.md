# Magento 2 - Vanilla #

##Installation

* Clone this repository
* Set proper permissions for Warp Framework:
```
chmod ug+rwx ./warp
```
* initialize project
```
./warp init 
```
* Start project
```
./warp start 
```
* Install composer dependencies:  
```
warp composer install 
```
* Set proper permissions for Magento console app:
```
chmod ug+x bin/magento
```
* Add rule /etc/hosts file: 
```
127.0.0.1	local.magento-vanilla.com
```
* Install Magento
```
warp magento setup:install --base-url=http://local.magento-vanilla.com --db-host=mysql --db-name=m2_vanilla_db --db-user=m2_vanilla_user --db-password=m2_vanilla_password --backend-frontname=admin --admin-firstname=admin --admin-lastname=admin --admin-email=admin@mail.com --admin-user=admin --admin-password=123123q --language=en_US --currency=USD --timezone=America/Argentina/Buenos_Aires --use-rewrites=1 --cleanup-database
```
* Set developer mode
```
warp magento deploy:mode:set developer
```
