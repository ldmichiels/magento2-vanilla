<?php

namespace Acme\Semaforo\Console\Command;

use Acme\Semaforo\Api\Data\SemaforoInterface;
use Acme\Semaforo\Api\SemaforoRepositoryInterface;
use Acme\Semaforo\Helper\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Console\Cli;

/**
 * Class ImportSkuColorsCommand
 * @package Acme\Semaforo\Console\Command
 */
class ImportSkuColorsCommand extends Command
{
    /**
     * @var Connection
     */
    protected $helperConnection;

    /**
     * @var \Acme\Semaforo\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Acme\Semaforo\Api\Data\SemaforoInterfaceFactory
     */
    protected $semaforoFactory;

    /**
     * @var \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory
     */
    protected $semaforoRepositoryFactory;

    /**
     * ImportSkuColorsCommand constructor.
     * @param Connection $helperConnection
     * @param \Acme\Semaforo\Helper\Data $helperData
     * @param \Acme\Semaforo\Api\Data\SemaforoInterfaceFactory $semaforoFactory
     * @param \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory $semaforoRepositoryFactory
     */
    public function __construct(
        \Acme\Semaforo\Helper\Connection $helperConnection,
        \Acme\Semaforo\Helper\Data $helperData,
        \Acme\Semaforo\Api\Data\SemaforoInterfaceFactory $semaforoFactory,
        \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory $semaforoRepositoryFactory
    ) {
        parent::__construct();

        $this->helperConnection = $helperConnection;
        $this->helperData = $helperData;
        $this->semaforoFactory = $semaforoFactory;
        $this->semaforoRepositoryFactory = $semaforoRepositoryFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('catalog:product:semaforo')
            ->setDescription('Import relationship between Sku and Color');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            // Import
            $output->writeln('<info>Importing data</info>');
            /* @var $semaforo_data array */
            $semaforo_data = $this->helperConnection->getSemaforoData();
            $count = count($semaforo_data);
            // Clean relatioship table
            if ($count == 0) {
                throw new \Exception("There is no imported data!");
            }
            $output->writeln('<info>There is '.$count.' rows imported.</info>');
            $output->writeln('<info>Cleaning old data</info>');
            $this->helperData->cleanSemaforoTable();

            /* @var SemaforoRepositoryInterface */
            $semaforoRepository = $this->semaforoRepositoryFactory->create();

            // loop imported data
            $output->writeln('<info>Loading new data, please wait...</info>');
            foreach ($semaforo_data as $row) {
                // validate data
                if (!isset($row['sku'], $row['color']) ||
                    !$this->helperData->validateSku($row['sku'])
                ) {
                    continue;
                }
                // Create new entries
                /* @var $semaforo SemaforoInterface */
                $semaforo = $this->semaforoFactory->create();
                $semaforo->setSku($row['sku']);
                $semaforo->setColor($row['color']);

                // Persist changes
                $semaforoRepository->save($semaforo);

                unset($semaforo);
            }
            $output->write('<info>Success!</info>');

        } catch(\Exception $e) {
            $output->writeln('<error>'.$e->getMessage().'</error>');
        }
        $output->writeln('<info>Process ends.</info>');

        return Cli::RETURN_SUCCESS;
    }
}
