<?php

namespace Acme\Semaforo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Acme\Semaforo\Api\Data\SemaforoInterface;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->productRepository = $productRepository;
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context);
    }

    /**
     * Check if product exists
     * @param $sku
     * @return bool
     */
    public function validateSku($sku)
    {
        try {
            $this->productRepository->get($sku);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Delete all elements of the table
     * @return $this|null
     */
    public function cleanSemaforoTable()
    {
        try {
            $connection = $this->resourceConnection->getConnection();
            $connection->truncateTable(SemaforoInterface::TABLE);
            $this->getLogger()->info(SemaforoInterface::TABLE ." table truncated.");
        } catch (\Exception $e) {
            $connection->rollBack();
            $this->getLogger()->error($e->getMessage());
            return null;
        }
        return $this;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->_logger;
    }
}