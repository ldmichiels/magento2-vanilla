<?php

namespace Acme\Semaforo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Connection extends AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    public function getSemaforoData()
    {
        // Here we emulate the behavior of the HTTP connection to consume an external service
        return [
            ['sku' => 'test_product_001', 'color' => '000'],
            ['sku' => 'test_product_002', 'color' => '00F'],
            ['sku' => 'test_product_003', 'color' => '0F0'],
            ['sku' => 'test_product_004', 'color' => 'F00'],
            ['sku' => 'test_product_005', 'color' => 'FFF'],
        ];
    }
}