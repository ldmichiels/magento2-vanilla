<?php

namespace Acme\Semaforo\Plugin;

use Acme\Semaforo\Api\Data\SemaforoInterface;
use Acme\Semaforo\Api\SemaforoRepositoryInterface;

class AddColorToProductPlugin
{
    /**
     * @var \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory
     */
    protected $semaforoRepositoryFactory;

    /**
     * AddColorToProductPlugin constructor.
     * @param \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory $semaforoRepositoryFactory
     */
    public function __construct(
        \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory $semaforoRepositoryFactory
    ) {
        $this->semaforoRepositoryFactory = $semaforoRepositoryFactory;
    }

    /**
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $subject
     * @param \Magento\Catalog\Api\Data\ProductInterface $entity
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function afterGet(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $entity
    ) {
        return $this->addExtensionAttributesToProduct($entity);
    }

    /**
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $subject
     * @param \Magento\Catalog\Api\Data\ProductInterface $entity
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function afterGetById(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $entity
    ) {
        return $this->addExtensionAttributesToProduct($entity);
    }

    /**
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $subject
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
    public function afterGetList(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductSearchResultsInterface $searchCriteria
    ) {
        $products = [];
        foreach ($searchCriteria->getItems() as $entity) {
            $products[] = $this->addExtensionAttributesToProduct($entity);;
        }
        $searchCriteria->setItems($products);
        return $searchCriteria;
    }

    public function afterSave
    (
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $entity
    ) {
        $extensionAttributes = $entity->getExtensionAttributes(); /** get current extension attributes from entity **/
        $color = $extensionAttributes->getColor();

        /* @var $semaforoRepository SemaforoRepositoryInterface  */
        $semaforoRepository = $this->semaforoRepositoryFactory->create();
        /* @var $semaforo SemaforoInterface */
        $semaforo = $semaforoRepository->get($entity->getSku());
        $semaforo->setColor($color);
        $semaforoRepository->save($semaforo);

        return $entity;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    private function addExtensionAttributesToProduct($product)
    {
        /* @var $semaforoRepository SemaforoRepositoryInterface  */
        $semaforoRepository = $this->semaforoRepositoryFactory->create();
        /* @var $semaforo SemaforoInterface */
        $semaforo = $semaforoRepository->get($product->getSku());

        $extensionAttributes = $product->getExtensionAttributes();
        $extensionAttributes->setColor($semaforo->getColor());
        $product->setExtensionAttributes($extensionAttributes);

        return $product;
    }

}