<?php

namespace Acme\Semaforo\Block\Product\View;

use Magento\Catalog\Model\Product;
use Acme\Semaforo\Api\Data\SemaforoInterface;

/**
 * Class Semaforo
 * @package Magento\Catalog\Block\Product\View
 */
class Semaforo extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Product
     */
    protected $product = null;

    /**
     * @var \Magento\Framework\Registry|null
     */
    protected $coreRegistry = null;

    /**
     * @var \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory
     */
    protected $semaforoRepositoryFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Acme\Semaforo\Api\SemaforoRepositoryInterfaceFactory $semaforoRepositoryFactory,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->semaforoRepositoryFactory = $semaforoRepositoryFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->product) {
            $this->product = $this->coreRegistry->registry('product');
        }
        return $this->product;
    }

    /**
     * @param $sku
     * @return string
     */
    public function getSkuColor($sku)
    {
        /* @var $semaforoRepository \Acme\Semaforo\Api\SemaforoRepositoryInterface */
        $semaforoRepository = $this->semaforoRepositoryFactory->create();
        /* @var $semaforo SemaforoInterface */
        $semaforo = $semaforoRepository->get($sku);
        return $semaforo->getColor();
    }
}
