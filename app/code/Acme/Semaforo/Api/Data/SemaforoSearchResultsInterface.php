<?php

namespace Acme\Semaforo\Api\Data;

interface SemaforoSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Acme\Semaforo\Api\Data\SemaforoInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Acme\Semaforo\Api\Data\SemaforoInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
