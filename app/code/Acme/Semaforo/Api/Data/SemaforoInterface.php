<?php

namespace Acme\Semaforo\Api\Data;

interface SemaforoInterface
{
    const TABLE = "test_catalog_product_semaforo";

    const ID = "id";

    const COLOR = "color";

    const SKU = "sku";

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getColor();

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color);

    /**
     * @return string
     */
    public function getSku();

    /**
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);
}