<?php

namespace Acme\Semaforo\Api;

use \Acme\Semaforo\Api\Data\SemaforoInterface;

interface SemaforoRepositoryInterface
{
    /**
     * @param Data\SemaforoInterface $semaforo
     * @return SemaforoInterface
     */
    public function save(SemaforoInterface $semaforo);

    /**
     * Get info about semaforo by product SKU
     *
     * @param $sku
     * @return SemaforoInterface
     */
    public function get($sku);

    /**
     * Get info about semaforo by id
     *
     * @param $productId
     * @return SemaforoInterface
     */
    public function getById($id);

    /**
     * Delete semaforo
     *
     * @param SemaforoInterface $semaforo
     * @return bool Will returned True if deleted
     */
    public function delete(SemaforoInterface $semaforo);

    /**
     * Get product list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Acme\Semaforo\Api\Data\SemaforoSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
}