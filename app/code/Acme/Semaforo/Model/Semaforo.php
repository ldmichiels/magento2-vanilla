<?php

namespace Acme\Semaforo\Model;

use \Magento\Framework\Model\AbstractExtensibleModel;
use \Acme\Semaforo\Api\Data\SemaforoInterface;

class Semaforo extends AbstractExtensibleModel implements SemaforoInterface
{
    protected function _construct()
    {
        $this->_init(\Magento\Catalog\Model\ResourceModel\Product::class);
    }

    /**
     * @inheritDoc
     */
    public function getColor()
    {
        return $this->getData(SemaforoInterface::COLOR);
    }

    /**
     * @inheritDoc
     */
    public function setColor($color)
    {
        $this->setData(SemaforoInterface::COLOR, $color);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->getData(SemaforoInterface::SKU);
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        $this->setData(SemaforoInterface::SKU, $sku);
        return $this;
    }
}