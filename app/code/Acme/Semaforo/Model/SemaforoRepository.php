<?php

namespace Acme\Semaforo\Model;

use Acme\Semaforo\Api\Data;
use Acme\Semaforo\Api\Data\SemaforoInterface;
use Acme\Semaforo\Api\SemaforoRepositoryInterface;

class SemaforoRepository implements SemaforoRepositoryInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Acme\Semaforo\Api\Data\SemaforoInterfaceFactory
     */
    protected $semaforoFactory;

    /**
     * @var \Acme\Semaforo\Model\ResourceModel\Semaforo
     */
    protected  $semaforoResourceModel;

    /**
     * @var \Acme\Semaforo\Model\ResourceModel\Semaforo\CollectionFactory
     */
    protected $semaforoCollectionFactory;

    /**
     * @var \Acme\Semaforo\Api\Data\SemaforoSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * SemaforoRepository constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param Data\SemaforoInterfaceFactory $semaforoFactory
     * @param ResourceModel\Semaforo $semaforoResourceModel
     * @param ResourceModel\Semaforo\Collection $semaforoCollectionFactory
     * @param Data\SemaforoSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Acme\Semaforo\Api\Data\SemaforoInterfaceFactory $semaforoFactory,
        \Acme\Semaforo\Model\ResourceModel\Semaforo $semaforoResourceModel,
        \Acme\Semaforo\Model\ResourceModel\Semaforo\Collection $semaforoCollectionFactory,
        \Acme\Semaforo\Api\Data\SemaforoSearchResultsInterfaceFactory $searchResultsFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
    ) {
        $this->logger = $logger;
        $this->semaforoFactory = $semaforoFactory;
        $this->semaforoResourceModel = $semaforoResourceModel;
        $this->semaforoCollectionFactory = $semaforoCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritdoc
     */
    public function save(SemaforoInterface $semaforo)
    {
        try {
            $this->semaforoResourceModel->save($semaforo);
        } catch (\Exception $e) {
            $this->logger->error("Semaforo can't be saved");
            throw $e;
        }
        return $semaforo;
    }

    /**
     * @inheritdoc
     */
    public function get($sku)
    {
        try {
            /* @var $semaforo SemaforoInterface */
            $semaforo = $this->semaforoFactory->create();
            $this->semaforoResourceModel->load($semaforo, $sku, SemaforoInterface::SKU);
        } catch(\Exception $e) {
            $this->logger->error("Error trying to get semaforo with SKU $sku");
            throw $e;
        }
        return $semaforo;
    }

    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        try {
            /* @var $semaforo SemaforoInterface */
            $semaforo = $this->semaforoFactory->create();
            $this->semaforoResourceModel->load($semaforo, $id, SemaforoInterface::ID);
        } catch(\Exception $e) {
            $this->logger->error("Error trying to get semaforo with ID $id");
            throw $e;
        }
        return $semaforo;
    }

    /**
     * @inheritdoc
     */
    public function delete(SemaforoInterface $semaforo)
    {
        try {
            $this->semaforoResourceModel->delete($semaforo);
            return true;
        } catch (\Exception $e) {
            $this->logger->error("Semaforo can not be deleted");
        }
        return false;
    }

    /* @inheritdoc */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Acme\Semaforo\Model\ResourceModel\Semaforo\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $collection->load();

        /* @var \Acme\Semaforo\Api\Data\SemaforoSearchResultsInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }

}