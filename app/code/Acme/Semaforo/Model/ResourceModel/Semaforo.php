<?php

namespace Acme\Semaforo\Model\ResourceModel;

use Acme\Semaforo\Api\Data\SemaforoInterface;

class Semaforo extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init(SemaforoInterface::TABLE, SemaforoInterface::ID);
    }
}