<?php

namespace Acme\Semaforo\Model\ResourceModel\Semaforo;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            \Acme\Semaforo\Model\Semaforo::class,
            \Acme\Semaforo\Model\ResourceModel\Semaforo::class
        );
    }
}